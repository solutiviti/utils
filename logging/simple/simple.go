package simple
import (
	"git.cps.pf/team42/utils/logging"
	"log"
	"git.cps.pf/team42/utils/terminal"
	"time"
	"fmt"
	"strings"
	"os"
)

type simpleLoggerDefinition struct {
	level   logging.LogLevel
	verbose bool
}

func SimpleLogger() logging.Logger {
	return &simpleLoggerDefinition{
		level: logging.InfoLevel,
		verbose: false,
	}
}

func init() {
	logging.New = SimpleLogger
}

func (s *simpleLoggerDefinition) Trace(message string, a ...interface{}) {
	withLogEnv(s, logging.TraceLevel, func() {
		log.Printf(message, a...)
	})

}

func (s *simpleLoggerDefinition) Debug(message string, a ...interface{}) {
	withLogEnv(s, logging.DebugLevel, func() {
		log.Printf(message, a...)
	})
}

func (s *simpleLoggerDefinition) Info(message string, a ...interface{}) {
	withLogEnv(s, logging.InfoLevel, func() {
		log.Printf(message, a...)
	})
}

func (s *simpleLoggerDefinition) Warn(message string, a ...interface{}) {
	withLogEnv(s, logging.WarnLevel, func() {
		log.Printf(message, a...)
	})
}

func (s *simpleLoggerDefinition) Error(message string, a ...interface{}) {
	withLogEnv(s, logging.ErrorLevel, func() {
		log.Printf(message, a...)
	})
}

func (s *simpleLoggerDefinition) Fatal(message string, a ...interface{}) {
	withLogEnv(s, logging.FatalLevel, func() {
		log.Printf(message, a...)
	})

	os.Exit(1)
}

func (s *simpleLoggerDefinition) SetLevel(level logging.LogLevel) {
	s.level = level
}

func (s *simpleLoggerDefinition) ActivateVerboseOutput(verboseFlag bool) {
	s.verbose = verboseFlag
}

func withLogEnv(s *simpleLoggerDefinition, level logging.LogLevel, exec func()) {
	if level >= s.level {
		outputDate := prettyPrintDate(s)
		outputLevel := prettyPrintLevel(level)
		prefix := fmt.Sprintf(" %s ", outputLevel)

		if s.verbose {
			outputDate = prettyPrintDate(s, "13:00:12.132")
			prefix = fmt.Sprintf("%s %s ", outputDate, outputLevel)
		}

		log.SetPrefix(prefix)
		log.SetFlags(0)

		exec()
	}
}

func prettyPrintLevel(level logging.LogLevel) string {
	var color string

	switch level {
	case logging.TraceLevel:
		color = terminal.YELLOW
	case logging.DebugLevel:
		color = terminal.CYAN
	case logging.InfoLevel:
		color = terminal.BLUE
	case logging.WarnLevel:
		color = terminal.MAGENTA
	case logging.ErrorLevel:
		color = terminal.RED
	case logging.FatalLevel:
		color = terminal.RED
	}

	prettyLevel := fmt.Sprintf("%-5s", strings.ToUpper(level.String()))
	output := terminal.Colorize(prettyLevel, color)
	return output
}

func prettyPrintDate(s *simpleLoggerDefinition, format ...string) string {
	currentTime := time.Now().Local()

	var formattedTime = currentTime.Format("132")

	if len(format) > 0 {
		formattedTime = currentTime.Format(format[0])
	}

	output := terminal.Colorize(strings.Join([]string{"[",formattedTime,"]"},""), terminal.FAINT)
	return output
}