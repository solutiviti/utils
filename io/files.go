package io
import (
	"os"
)

func DirectoryExists(directory string) bool {
	if _, err := os.Stat(directory); os.IsNotExist(err) {
		return false
	}

	return true
}

func FileExists(file string) bool {
	if _, err := os.Stat(file); err != nil {
		return false
	}

	return true
}

func CanReadFile(file string) bool {
	if _, err := os.Open(file); err != nil {
		return false
	}

	return true
}
