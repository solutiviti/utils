package terminal
import (
	"fmt"
	"os/exec"
	"regexp"
	"errors"
	versionCompare "github.com/mcuadros/go-version"
	"strings"
)

const (
	YES = "YES"
	NO = "NO"
)

// AskForConfirmation asks a question for the user to confirm
func AskForConfirmation(question string) string  {
	answer := "2"
	fmt.Println(question)
	fmt.Println("1) Yes")
	fmt.Println("2) No")
	fmt.Print("> ")
	fmt.Scanln(&answer)
	switch answer {
	case "1":
		return YES
	case "2":
		return NO
	}

	return NO
}

func Announce(message string, args ...interface{}) {
	announcement := "# " + fmt.Sprintf(message, args...) + " #"
	underline := LeftPad("", "#", len(announcement))

	fmt.Println()
	fmt.Println(underline)
	fmt.Println(announcement)
	fmt.Println(underline)
	fmt.Println()
}

func LeftPad(s string, padStr string, pLen int) string{
	return strings.Repeat(padStr, pLen) + s
}

// Requires will check if a command exists
func Requires(commandName string, arguments ...string) (string, error) {

	numberOfArguments := len(arguments)
	if numberOfArguments > 0 {
		var version, operator, flags, commandStr string

		if numberOfArguments >= 3 {
			flags = arguments[0]
			operator = arguments[1]
			version = arguments[2]
			commandStr = commandName+" "+flags

		} else {
			version = arguments[0]
			commandStr = commandName
		}

		outputBytes, err := exec.Command("bash", "-c", commandStr).Output()

		// extract version 2 check
		// example: 1.12.10
		r, _ := regexp.Compile("([0-9]+.[0-9]+)(.[0-9]+)")
		versionSlice := r.FindAllString(string(outputBytes), -1)
		actualVersion := ""
		if versionSlice != nil {
			actualVersion = versionSlice[0]
		} else {
			return "KO", errors.New("Cannot compare versions")
		}

		// check against reference version
		if err == nil {
			compResult := versionCompare.Compare(actualVersion, version, operator)

			if compResult == true {
				return "OK", nil
			} else {
				return "KO", errors.New("Requirement for " + commandName + " is " + operator + " " + version)
			}

		}


	} else {
		_, err := exec.Command("bash", "-c", commandName).Output()

		if err != nil {
			return "KO", err
		} else {
			return "OK", nil
		}
	}

	return "KO", errors.New("Unable to check requirement")
}

